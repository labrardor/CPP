#include<iostream>
#include<vector>
using namespace std;

#define ADD(a,b)((a)+(b))

inline int Add(int a, int b)
{
	return a + b;
}

int& func()
{
	int a = 0;
	return a;
}

int main()
{

	int c = ADD(4, 5);
	cout << c << endl;

	int a = 0;
	int b = 10;
	int*const p = &a;//引用的底层更像是指针常量：不能修改指向，可以修改指向的值
	*p = 20;
	
	return 0;
}