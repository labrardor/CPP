#pragma once
#include<assert.h>



namespace mystl
{
	class string 
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;//可以更改指向，但不能更改指向内容的值

		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin()const
		{
			return _str;
		}
		const_iterator end() const
		{
			return _str + _size;
		}

		//运算符重载
		char& operator[](size_t pos)
		{
			return _str[pos];
		}
		//修饰引用，让引用不能被修改
		const char& operator[](size_t pos) const
		{
			return _str[pos];
		}

		
		const char* c_str()
		{
			return _str;
		}

		size_t size() const
		{
			return _size;
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* temp = new char[n + 1];// + 1 留给'\0'
				strcpy(temp, _str);
				delete[] _str;
				_str = temp;
				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			//如果无空间/满了 先扩容
			if (_size == _capacity)
			{
				size_t new_capacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(new_capacity);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		void append(const char* str)
		{
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			strcpy(_str + _size, str);
			_size += len;
		}

		//插入一个字符
		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			///如果满了/空白 先扩容
			if (_size == _capacity)
			{
				size_t new_capacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(new_capacity);
			}
			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				--end;
			}
			_str[pos] = ch;
			_size++;

		}

		//插入一个串
		void insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			int end = _size;

			while (end >= (int)pos)
			{
				_str[end + len] = _str[end];
				--end;
			}

			strncpy(_str + pos, str, len);
			_size += len;
		}

		void erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || pos + len >= _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}

		}

		//添加一个字符
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		//添加一个串
		string& operator+=(const char* str)
		{
			append(str);

			return *this;
		}

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}


		string substr(size_t pos = 0, size_t len= npos)
		{
			assert(pos < _size);
			size_t end = pos + len;
			if (len == npos || pos + len >= _size)
			{
				end = _size;
			}
			string str;
			str.reserve(end - pos);
			for (size_t i = pos; i < end; i++)
			{
				str += _str[i];
			}
			return str;
		}

		size_t find(char ch, size_t pos = 0)
		{
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}

			return npos;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			const char* ptr = strstr(_str + pos, str);
			if (ptr == nullptr)
			{
				return npos;
			}
			else
			{
				return ptr - _str;
			}
		}


		//两个构造函数

		// string s2(s1);
		string(const string& s)
		{
			_size = s._size;
			_capacity = s._capacity;
			_str = new char[_capacity + 1];// + 1 的位置留给'\0'
			strcpy(_str, s._str);//拷贝的时候，会将s._str中末尾的'\0'也拷贝过去
		}
		//string s1 = "hello world!"
		string(const char* str = "")//自带缺省值
		{
			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity + 1];// +1 的位置留给'\0'
			strcpy(_str, str);
		}

		string& operator=(const string& s)
		{
			if (this != &s)
			{
				char* temp = new char[s._capacity + 1];
				strcpy(temp, s._str);
				delete[] _str;
				_str = temp;
				_size = s._size;
				_capacity = s._capacity;
			}
			return *this;
		}


		~string()
		{
			_size = 0;
			_capacity = 0;
			delete[] _str;
			_str = nullptr;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		const static size_t npos = -1;//size_t : unsized long long 整形最大值
	};


	void test()
	{
		string s1("hhhhh");
		s1 = "sdsasassfaawfrghs";
		string s2 = "ashadhd";
		s2 += "kkk";
		s1.swap(s2);
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
	}



}