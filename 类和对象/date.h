#pragma once

#include<iostream>

using namespace std;


class date {
public:
	date()
	{
		_year = 1970;
		_month = 1;
		_day = 1;
	}
	date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	~date()
	{

	}


	bool operator<(const date& d);
	bool operator<=(const date& d);
	bool operator>=(const date& d);
	bool operator>(const date& d);
	bool operator==(const date& d);
	bool operator!=(const date& d);
	date& operator=(const date& d);


	void Print();


private:
	int _year;
	int _month;
	int _day;
};

