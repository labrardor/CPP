#include"date.h"

//date运算符重载区
bool date::operator<(const date& d)
{
	if (_year < d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month < d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			if (_day < d._day)
			{
				return true;
			}
		}
	}
	return false;
}

bool date::operator<=(const date& d)
{
	//if (_year <= d._year)
	//{
	//	return true;
	//	if (_month <= d._month)
	//	{
	//		return true;
	//		if (_day <= d._day)
	//		{
	//			return true;
	//		}
	//	}
	//}
	//return false;
	return *this < d || *this == d;
}
bool date::operator>=(const date& d)
{
	//if (_year >= d._year)
	//{
	//	return true;
	//	if (_month >= d._month)
	//	{
	//		return true;
	//		if (_day >= d._day)
	//		{
	//			return true;
	//		}
	//	}
	//}
	//return false;
	return *this > d || *this == d;
}

bool date::operator>(const date& d)
{
	//if (_year > d._year)
	//{
	//	return true;
	//}
	//else if (_year == d._year)
	//{
	//	if (_month > d._month)
	//	{
	//		return true;
	//	}
	//	else if (_month == d._month)
	//	{
	//		if (_day > d._day)
	//		{
	//			return true;
	//		}
	//	}
	//}
	//return false;
	return !(*this < d || *this == d);
}

bool date::operator ==(const date& d)
{
	return _year == d._year && _month == d._month && _day == d._day;
}

bool date::operator!=(const date& d)
{
	return !(*this == d);
}

date& date::operator=(const date& d)//如果不加&,会进行船只返回
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
	return *this;
}




void date::Print()
{
	cout << _year << "-" << _month << "-" << _day << endl;
}




class Stack {
public:
	Stack()
	{
		_arr = (int*)malloc(sizeof(int) * 4);
		_size = 0;
		_capacity = 4;
	}
	Stack(const Stack& s)
	{
		int* temp = (int*)malloc(sizeof(int) * s._capacity);
		if (!temp)
		{
			perror("malloc fail!");
			exit(-1);
		}
		memcpy(temp, s._arr, s._capacity * sizeof(int));
		_arr = temp;
		_size = s._size;
		_capacity = s._capacity;
	}

private:
	int* _arr;
	int _size;
	int _capacity;
};

//int main()
//{
//	//date d1(122,5,7);
//	//d1.Print();
//
//	Stack s1;
//	Stack s2(s1);
//
//	date d1;
//	date d2;
//	date d3(2004,8,23);
//	//d1.Print();
//	//d2.Print();
//
//	cout << (d3 > d2) << endl;
//
//	//d3.Print();
//	const int x = 10;
//	
//
//	return 0;
//}