#pragma once
#include<iostream>
#include<assert.h>

using namespace std;

namespace stl
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		
		vector()
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{}
		
		//vector(const vector<T>& v)
		//{
		//	_start = new T[v.capacity()];
		//	memcpy(_start, v._start, v.size() * sizeof(T));
		//	_finish = _start + v.size();
		//	_endofstorage = _start + v.capacity();
		//}

		//现代实现
		//   v2(v1)
		vector(const vector<T>& v)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			resserve(v.capacity());
			for (const auto& e : v)
			{
				push_back(e);
			}
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstorage, v._endofstorage);
		}

		// v1 = v3
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}
		//迭代器们
		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin()const
		{
			return _start;
		}
		const_iterator end()const
		{
			return _finish;
		}


		//重载
		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}
		const T& operator[](size_t pos)const
		{
			assert(pos < size());
			return _start[pos];
		}

		//扩容
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t old = size();

				T* temp = new T[n];
				if (_start)//原先有数据
				{
					//memcpy(temp, _start, old * sizeof(T));//拷贝string这样的会出事
					for (int i = 0; i < old; i++)
					{
						temp[i] = _start[i];
					}
				}
				delete[] _start;

				_start = temp;
				_finish = _start + old;
				_endofstorage = _start + n;

			}
		}

		void resize(size_t n, T val = T())
		{
			if (n > size())
			{
				reserve(n);
				while (_finish < _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
			else
			{
				_finish = _start + n;
			}

		}

		//连续插
		//指向连续物理空间的指针就是天然的迭代器
		template <class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		vector(int n, const T& value)
		{
			resize(n, val);
		}

		//普通插
		void insert(iterator pos, const T& x)
		{
			assert(pos >= _start);
			assert(pos <= _finish);
			
			if (_finish == _endofstorage)
			{
				size_t len = pos - _start;//迭代器失效
				reserve(capacity() == 0 ? 4 : capacity() * 2);
				pos = begin() + len;
			}
			
			//memmove(pos + 1, pos, sizeof(T)*(_finish - pos) );
			
			iterator end = finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}

			*pos = x;
			++_finish;
		}

		iterator erase(iterator pos)
		{
			iterator it = pos + 1;
			assert(pos >= _start);
			assert(pos < _finish);

			while (it < _finish)
			{
				*(it - 1) = *it;
				it++;
			}
			--_finish;
			return pos;
		}

		//尾插
		void push_back(const T& x)
		{
			if (_finish == _endofstorage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}
			*_finish = x;
			++_finish;
		}
		
		//尾删
		void pop_back()
		{
			assert(size() > 0);
				--_finish;
		}




		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity()
		{
			return _endofstorage - _start;
		}



		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = nullptr;
				_finish = nullptr;
				_endofstorage = nullptr;
			}
		}

	private:  
		iterator _start;
		iterator _finish;
		iterator _endofstorage;
	};


	void print_vector(const vector<int>& v)
	{
		for (auto x : v)
		{
			cout << x << " ";
		}
		cout << endl;
	}
	void testvector()
	{
		vector<int>v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);
		v.push_back(6);

		v.pop_back();
		v.insert(v.begin(), 100);
		v.insert(v.begin(), 200);
		v.insert(v.begin(), 260);
		print_vector(v);
	}
}


